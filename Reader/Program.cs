﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace Reader
{
   public class VerticalReader
    {
        String Content;
        int Index = 0;
        double PageWidth;
        double PageHeight;
        VerticalReadingPage RightPage;
        VerticalReadingPage LeftPage;

        public VerticalReader(double PageW, double PageH, double FontSize, Grid[] ParentGrid, String Content)
        {
            PageWidth = PageW;
            PageHeight = PageH;
            this.Content = Content;
            RightPage = new VerticalReadingPage(PageW, PageH, FontSize, ParentGrid[0]);
            LeftPage = new VerticalReadingPage(PageW, PageH, FontSize, ParentGrid[1]);
        }

        public void OnPageSizeChanged(double PageW, double PageH)
        {
            int i = 0;
            if (RightPage != null)
            {
                RightPage.OnPageSizeChanged(this.Content, PageW, PageH);
                i = RightPage.InsertContent(Content, this.Index);
            }
            if (LeftPage != null)
            {
                LeftPage.OnPageSizeChanged(this.Content, PageW, PageH);
                i = LeftPage.InsertContent(Content, i);
            }
        }

        public void NextPage()
        {
            int Index = LeftPage.GetEndIndex();
            if (Index >= Content.Length)
            {
                //if there is no more content do not go to next page
            }
            else
            {
                this.Index = Index;
                RightPage.InsertContent(Content, Index);
                Index = RightPage.GetEndIndex();
                LeftPage.InsertContent(Content, Index);
            }
        }
        public void PreviousPage()
        {
            int Ptr = RightPage.GetStartIndex() - 1;
            int WordCounter = 0;
            int PtrOffset = 0;
            int NumOfLines = 0;
            bool OutOfRange = false;
            bool NextLineFoundYet = false;

            while (NumOfLines < RightPage.NumOfCol + LeftPage.NumOfCol)
            {
                if (Ptr - PtrOffset < 0)
                {
                    GoToFirstPage();
                    OutOfRange = true;
                    break;
                }

                if (Content[Ptr - PtrOffset].ToString() == "\n")
                {
                    NumOfLines++;
                    WordCounter = 0;
                    NextLineFoundYet = true;
                }
                else
                {
                    WordCounter++;
                }
                if (WordCounter >= LeftPage.NumOfRow)
                {
                    if (NextLineFoundYet == true)
                    {
                        NumOfLines++;
                        WordCounter = 0;
                    }
                    else
                    {
                        NumOfLines += 2;
                        WordCounter = 0;
                    }
                }
                if (NumOfLines < RightPage.NumOfCol + LeftPage.NumOfCol)
                {
                    PtrOffset++;
                }
            }

            WordCounter = 0;
            bool NextLineFound = false;
            int NextLineLocation = 0;
            int PtrOffsetTemp = 1;
            if (!OutOfRange)
            {
                while (WordCounter < LeftPage.NumOfRow - 1)
                {
                    if ((Ptr - PtrOffset - PtrOffsetTemp) < 0)
                    {
                        GoToFirstPage();
                        OutOfRange = true;
                        break;
                    }
                    if (Content[Ptr - PtrOffset - PtrOffsetTemp].ToString() == "\n")
                    {
                        NextLineFound = true;
                        NextLineLocation = PtrOffsetTemp;
                        break;
                    }
                    WordCounter++;
                    PtrOffsetTemp++;
                }
            }
            if (!OutOfRange)
            {
                if (NextLineFound)
                {
                    PtrOffset += NextLineLocation - 1; //go to char before next line char
                }
                else
                {
                    PtrOffset += LeftPage.NumOfRow - 1;
                }

                if (Ptr - PtrOffset < 0)
                {
                    GoToFirstPage();
                }
                else
                {
                    LeftPage.InsertContent(Content, RightPage.InsertContent(Content, Ptr - PtrOffset));
                }
            }

        }
        void GoToFirstPage()
        {
            LeftPage.InsertContent(Content, RightPage.InsertContent(Content, 0));
            this.Index = 0;
        }
    }
   public  class VerticalReadingPage
    {
        double FontSize;
        double PageWidth;
        double PageHeight;
        public int NumOfCol;
        public int NumOfRow;
        public int NumOfElement;
        int NumOfCharDisplayed;
        Thickness PageThickness;
        Grid ParentGrid;
        int CurrentElementIndex = 0;

        public VerticalReadingPage(double PageW, double PageH, double FontSize, Grid ParentGrid)
        {
            this.FontSize = FontSize;
            PageWidth = PageW;
            PageHeight = PageH;

            this.NumOfCol = (int)(this.PageWidth * 2.0 / 5.0 / this.FontSize);
            this.NumOfRow = (int)(this.PageHeight * 5.5 / 10.0 / this.FontSize);
            this.NumOfElement = NumOfCol * NumOfRow;
            this.ParentGrid = ParentGrid;
        }
        public int InsertContent(String Content, int StartIndex)
        {
            ParentGrid.Children.Clear();
            int I = StartIndex;
            if (I < 0)
            {
                I = 0;
            }
            this.CurrentElementIndex = I;
            int Index = I;
            TextBlock[,] ElementInPage = new TextBlock[NumOfRow, NumOfCol];
            for (int Row = 0; Row < NumOfRow; Row++)
            {
                for (int Col = 0; Col < NumOfCol; Col++)
                {
                    ElementInPage[Row, Col] = new TextBlock();
                    ElementInPage[Row, Col].Text = "";
                    ElementInPage[Row, Col].HorizontalAlignment = HorizontalAlignment.Center;
                    ElementInPage[Row, Col].VerticalAlignment = VerticalAlignment.Center;
                    ElementInPage[Row, Col].FontSize = this.FontSize;

                    Grid.SetRow(ElementInPage[Row, Col], Row);
                    Grid.SetColumn(ElementInPage[Row, Col], Col);
                    ParentGrid.Children.Add(ElementInPage[Row, Col]);
                }
            }
            int NumberOfElement = 0;
            if (I > Content.Length)
            {
                //blank page if index exceed content length
            }
            else
            {
                NumOfCharDisplayed = 0;
                for (int Col = NumOfCol - 1; Col > -1; Col--)
                {
                    for (int Row = 0; Row < NumOfRow; Row++)
                    {
                        if (NumberOfElement < NumOfCol * NumOfRow && Index < Content.Length)
                        {
                            if (Content[Index].ToString() == "\n")
                            {
                                NumberOfElement += NumOfRow - Row;
                                NumOfCharDisplayed++;
                                Col--;
                                Row = -1;
                                Index++;
                            }
                            else
                            {
                                if (Row < NumOfRow && Col > -1)
                                {
                                    NumOfCharDisplayed++;
                                    ElementInPage[Row, Col].Text = Content[Index].ToString();
                                    Index++;
                                    NumberOfElement++;
                                }
                            }
                        }
                    }
                }
            }
            return Index;
        }
        public void OnPageSizeChanged(String Content, double PageWidth, double PageHeight)
        {
            this.PageWidth = PageWidth;
            this.PageHeight = PageHeight;
            ClearParentGrid();
            PopulateGrid();
        }
        public int GetStartIndex()
        {
            return CurrentElementIndex;
        }
        public int GetEndIndex()
        {
            return CurrentElementIndex + NumOfCharDisplayed;
        }
        private void PopulateGrid()
        {
            PageThickness = ParentGrid.Margin;
            PageThickness.Left = PageWidth / 10.0;
            PageThickness.Right = PageThickness.Left;
            PageThickness.Top = PageHeight * 1.5 / 10.0;
            PageThickness.Bottom = PageHeight * 1.0 / 10.0;
            ParentGrid.Margin = PageThickness;

            this.NumOfCol = (int)(PageWidth * 2.0 / 5.0 / this.FontSize);
            this.NumOfRow = (int)(PageHeight * 5.5 / 10.0 / this.FontSize);
            this.NumOfElement = NumOfCol * NumOfRow;


            ColumnDefinition[] ColInPage = new ColumnDefinition[NumOfCol];
            for (int i = 0; i < this.NumOfCol; i++)
            {
                ColInPage[i] = new ColumnDefinition();
                ParentGrid.ColumnDefinitions.Add(ColInPage[i]);
            }

            RowDefinition[] RowInPage = new RowDefinition[NumOfRow];
            for (int i = 0; i < NumOfRow; i++)
            {
                RowInPage[i] = new RowDefinition();
                ParentGrid.RowDefinitions.Add(RowInPage[i]);
            }

            TextBlock[,] ElementInPage = new TextBlock[NumOfRow, NumOfCol];
            for (int Row = 0; Row < NumOfRow; Row++)
            {
                for (int Col = 0; Col < NumOfCol; Col++)
                {
                    ElementInPage[Row, Col] = new TextBlock();
                    ElementInPage[Row, Col].Text = "";
                    ElementInPage[Row, Col].HorizontalAlignment = HorizontalAlignment.Center;
                    ElementInPage[Row, Col].VerticalAlignment = VerticalAlignment.Center;
                    ElementInPage[Row, Col].FontSize = 15;
                    Grid.SetRow(ElementInPage[Row, Col], Row);
                    Grid.SetColumn(ElementInPage[Row, Col], Col);
                    ParentGrid.Children.Add(ElementInPage[Row, Col]);
                }
            }
        }
        void ClearParentGrid()
        {
            this.ParentGrid.Children.Clear();
            this.ParentGrid.RowDefinitions.Clear();
            this.ParentGrid.ColumnDefinitions.Clear();
        }

    }
}
