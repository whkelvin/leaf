﻿using System.Windows;
using System.Windows.Controls;
using LeafReader;
using System.Data.SQLite;

namespace Leaf
{
    /// <summary>
    /// Interaction logic for ReadingPage.xaml
    /// </summary>
    public partial class ReadingPage : UserControl
    {
        VerticalReader  Display = null;
        Book CurrentBook = null;
        public ReadingPage()
        {
            InitializeComponent();
        }

        public ReadingPage(string Content, object BookToRead)
        {
            InitializeComponent();
            CurrentBook = (Book)BookToRead;
            Grid[] PageArray = new Grid[2];
            PageArray[0] = RightPage;
            PageArray[1] = LeftPage;
            Display = new VerticalReader(this.ActualWidth / 2.0, this.ActualHeight, 15.0, PageArray, Content);

            if (System.IO.File.Exists("LeafBookList.sqlite"))
            {
                SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                DBConnection.Open();
                string sql = "SELECT BookMarkIndex FROM BookList WHERE FileName='" + System.IO.Path.GetFileName(CurrentBook.FilePath) + "';";
                SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
                SQLiteDataReader Reader = cmd.ExecuteReader();
                int Size = 0;
                int IndexFromDB = 0;
                while (Reader.Read())
                {
                    // should only find one entry
                    Size++;
                    IndexFromDB = (int)Reader["BookMarkIndex"]; 
                }
                if (Size == 1 && IndexFromDB >=0)
                {
                    Display.GoToIndex(IndexFromDB);
                }
                DBConnection.Close();
            }


            this.SizeChanged += OnSizeChanged;
            NextPage.Click += OnNextPageClicked;
            PrevPage.Click += OnPrevPageClicked;

            PreviousUIPage.Click += GoToPreviousUIPage;
            //NextUIPage.Click += GoToNextUIPage;
            IncreaseFontSizeBtn.Click += IncreaseFontSize;
            DecreaseFontSizeBtn.Click += DecreaseFontSize;
            //BookMardBtn.Click += AddBookMarkEvent;
        }

        public ReadingPage(object BookToRead) {
            InitializeComponent();
        }

        void OnSizeChanged(object sender, RoutedEventArgs e)
        {
            Display.OnPageSizeChanged(this.ActualWidth / 2.0, this.ActualHeight);
        }

        void OnNextPageClicked(object sender, RoutedEventArgs e)
        {
            if (Display != null)
            {
                Display.NextPage();
            }
        }
        void OnPrevPageClicked(object sender, RoutedEventArgs e)
        {
            if (Display != null)
            {
                Display.PreviousPage();
            }
        }

        private void GoToPreviousUIPage(object sender, RoutedEventArgs e)
        {
            AddBookMark();
            PageSwitcher.Switch(new BookListPage());
        }
        private void GoToNextUIPage(object sender, RoutedEventArgs e)
        {
            //Display.IncreaseFontSize();
        }

        private void IncreaseFontSize(object sender, RoutedEventArgs e) {
            Display.IncreaseFontSize();
        }
        private void DecreaseFontSize(object sender, RoutedEventArgs e) {
            Display.DecreaseFontSize();
        }

        private void AddBookMarkEvent(object sender, RoutedEventArgs e)
        {
            AddBookMark();
        }
        private void AddBookMark()
        {
            if (System.IO.File.Exists("LeafBookList.sqlite"))
            {
                SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                DBConnection.Open();
                string sql = "Update BookList SET BookMarkIndex =" + Display.Index.ToString() + " WHERE FileName='" + System.IO.Path.GetFileName(CurrentBook.FilePath) + "';";
                SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
                cmd.ExecuteNonQuery();
                DBConnection.Close();
            }
        }

        ~ReadingPage()
        {
            AddBookMark();
        }
    }
}
