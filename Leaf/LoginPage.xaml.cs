﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Leaf
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class LoginPage : UserControl
    {
        public LoginPage()
        {
            InitializeComponent();
            this.Login_Next_Button.Click += LoginNextBtnClick;
        }

        private void LoginNextBtnClick(object sender, RoutedEventArgs e) {
            //PageSwitcher.Switch(new MainPage());
            PageSwitcher.Switch(new BookListPage());
        }
    }
}
