﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using EpubSharp;
using HtmlAgilityPack;
using Microsoft.Win32;
using System.Data.SQLite;
using System.Linq;

namespace Leaf
{
    /// <summary>
    /// Interaction logic for booklist.xaml
    /// </summary>
    public partial class BookListPage : UserControl
    {
        RadioButton[] BookItems;
        Book[] Books;
        public BookListPage()
        {
            InitializeComponent();
           
            //Create a sqlite db if it doesnt exist
            if (!System.IO.File.Exists("LeafBookList.sqlite"))
            {
                SQLiteConnection.CreateFile("LeafBookList.sqlite");
                SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                DBConnection.Open();
                string sql = "CREATE TABLE BookList (FileName varchar(50), Path varchar(200), BookMarkIndex int);";
                SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
                cmd.ExecuteNonQuery();
                DBConnection.Close();
            }
            else {
                SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Vertion=3;");
                DBConnection.Open();
                string sql = "SELECT * FROM BookList;";
                SQLiteCommand SQLCMD = new SQLiteCommand(sql, DBConnection);
                SQLiteDataReader reader = SQLCMD.ExecuteReader();
                int Length = 0;
                while (reader.Read())
                {
                    Length++;
                }
                if (Length > 0)
                {
                    ReadBtn.Visibility = Visibility.Visible;
                }
                else
                {
                    ReadBtn.Visibility = Visibility.Hidden;
                }
                BookItems = new RadioButton[Length];
                Books = new Book[Length];
                int i = 0;
                SQLCMD = new SQLiteCommand(sql, DBConnection);
                reader = SQLCMD.ExecuteReader();
                while (reader.Read()) {
                    Books[i] = new Leaf.Book("");
                    BookItems[i] = new RadioButton();

                    EpubBook eBook = EpubReader.Read(reader["Path"].ToString());
                    Books[i].Title = eBook.Title;
                    Books[i].FilePath = reader["Path"].ToString();
                    Books[i].Author = Enumerable.ToArray(eBook.Authors);
                    BookItems[i].Content = eBook.Title;
                    BookItems[i].Tag = Books[i];
                    BookItems[i].Style = this.FindResource("LeafToggleButton") as Style;
                    BookItems[i].Click += OnBookSelectionChange;
                    BookList.Children.Add(BookItems[i]);
                    i++;
                }
                try
                {
                    BookItems[0].IsChecked = true;
                    BookImageBorder.BorderThickness = new Thickness(1);
                    Book B = (Book)BookItems[0].Tag;
                    StringBuilder SB = new StringBuilder();
                    foreach (string Author in B.Author) {
                        SB.Append(Author);
                    }
                    BookImageBorder.Child = new BookImage(B.Title, SB.ToString());
                }
                catch (IndexOutOfRangeException e) {
                    //nothing is selected
                    BookImageBorder.BorderThickness = new Thickness(0);
                }

            }

            ReadBtn.Click += ReadBtnClick;
            PreviousUIPage.Click += GoToPreviousUIPage;
            AddBookBtn.Click += AddBook;
            DeleteBookBtn.Click += DeleteBook;

        }

        private void OnBookSelectionChange(object sender, EventArgs e)
        {
            RadioButton R = (RadioButton)sender;
            //check if book has a cover image
            Book B = (Book)R.Tag;
            StringBuilder SB = new StringBuilder();
            foreach (string author in B.Author){
                SB.Append(author);
            }
               BookImageBorder.Child = new BookImage(B.Title, SB.ToString());
        }

        private void ReadBtnClick(object sender, EventArgs e) {
            foreach (RadioButton  R in BookItems) {
                if (R.IsChecked == true) {
                    Book B = (Book)R.Tag;
                    EpubBook selectedBook = EpubReader.Read(B.FilePath);

                    //read html files to get content
                    var StrBuilder = new StringBuilder();
                    foreach (var Html in selectedBook.SpecialResources.HtmlInReadingOrder)
                    {
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(Html.TextContent);
                        foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//body//text()"))
                        {
                            StrBuilder.Append(node.InnerText.Trim(' '));
                            StrBuilder.Append('\n');
                        }
                    }
                    PageSwitcher.Switch(new ReadingPage(StrBuilder.ToString(), B));
                    return;
                }
            }
           
        }

        private void GoToPreviousUIPage(object sender, EventArgs e)
        {
            //PageSwitcher.Switch(new MainPage());
            PageSwitcher.Switch(new LoginPage());
        }

        private void AddBook(object sender, EventArgs e)
        {
            OpenFileDialog FileChooser = new OpenFileDialog();
            FileChooser.InitialDirectory = @"c:";
            FileChooser.Filter = "EPUB Files|*.epub";
            if (FileChooser.ShowDialog() == true)
            {
                string Path = FileChooser.FileName;
                string Filename = System.IO.Path.GetFileName(Path);
                string Dest = @"..\..\book";
                try
                {
                    File.Copy(Path, System.IO.Path.Combine(Dest, Filename), false);
                }
                catch (IOException) {
                    MessageBoxResult Result = MessageBox.Show("檔案已存在，確認覆蓋?", "Leaf Warning", MessageBoxButton.YesNo);
                    switch (Result)
                    {
                        case MessageBoxResult.Yes:
                            if (System.IO.File.Exists("LeafBookList.sqlite"))
                            { 
                                SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                                DBConnection.Open();
                                string sql = "DELETE FROM BookList WHERE FileName='" + Filename + "';";
                                SQLiteCommand SQLiteCMD = new SQLiteCommand(sql, DBConnection);
                                SQLiteCMD.ExecuteNonQuery();
                                DBConnection.Close();
                            }

                            File.Copy(Path, System.IO.Path.Combine(Dest, Filename), true);

                            break;
                        case MessageBoxResult.No:

                            break;        
                    }
                }
                if (System.IO.File.Exists("LeafBookList.sqlite"))
                {
                    SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                    DBConnection.Open();
                    string sql = "INSERT into BookList (FileName, Path, BookMarkIndex) values('" + Filename + "', '"+ System.IO.Path.Combine(Dest, Filename) + "', " + "0)";
                    SQLiteCommand SQLiteCMD = new SQLiteCommand(sql, DBConnection);
                    SQLiteCMD.ExecuteNonQuery();
                    DBConnection.Close();
                }
                else
                {
                    MessageBox.Show("db not found");
                }
            }

           

            PageSwitcher.Switch(new BookListPage());
        }

        private void DeleteBook(object sender, EventArgs e)
        {
            int Counter = 0;
            foreach (RadioButton R in BookItems)
            {
                if (R.IsChecked == true)
                {

                    Book B = (Book)R.Tag;
                    StringBuilder SB = new StringBuilder();
                    SB.Append("確定刪除 '");
                    SB.Append(B.Title);
                    SB.Append("'?");

                    MessageBoxResult Result = MessageBox.Show(SB.ToString(), "Leaf Warning", MessageBoxButton.YesNo);
                    switch (Result)
                    {
                        case MessageBoxResult.Yes:
                            if (File.Exists(B.FilePath))
                            {
                                File.Delete(B.FilePath);
                            }
                            SQLiteConnection DBConnection = new SQLiteConnection("Data Source=LeafBookList.sqlite;Version=3;");
                            DBConnection.Open();
                            string sql = "DELETE FROM BookList WHERE FileName='" + Path.GetFileName(B.FilePath) + "';";
                            SQLiteCommand SQLiteCMD = new SQLiteCommand(sql, DBConnection);
                            SQLiteCMD.ExecuteNonQuery().ToString();
                            PageSwitcher.Switch(new BookListPage());
                            break;
                        case MessageBoxResult.No:

                            break;
                    }
                    Counter++;
                }
            }
            if (Counter == 0) {
                MessageBox.Show("Please Select A Book!");
            }
        }

    }
}
