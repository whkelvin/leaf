﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leaf
{
    class Book
    {
        public string Title { get; set; }
        public string[] Author { get; set; }
        public string FilePath { get; set; }
        public string[] ChapterList { get; set; }
        public string Content { get; set; }

        public Book(string Title, string FilePath) {
            this.Title = Title;
            this.FilePath = FilePath;
        }

        public Book(string Content) {
            this.Content = Content;
        }
    }
}
