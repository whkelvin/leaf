﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Leaf
{
    public partial class PageSwitcher: Window
    {
        public static MainWindow initialWindow;

        public static void Switch(UserControl newPage) {
            initialWindow.Navigate(newPage);
        }

    }
}
