﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Leaf
{
    /// <summary>
    /// Interaction logic for BookImage.xaml
    /// </summary>
    public partial class BookImage : UserControl
    {
        public BookImage()
        {
            InitializeComponent();
        }

        public BookImage(string title, string author) {
            InitializeComponent();
            Title.Text = title;
            Author.Text = author;
        }
    }
}
