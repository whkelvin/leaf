﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Leaf
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            LibraryBtn.Click += LibraryBtnClick;
            PreviousUIPage.Click += GoToPreviousUIPage;
        }

        private void LibraryBtnClick(object sender, RoutedEventArgs e)
        {
            PageSwitcher.Switch(new BookListPage());
        }

        private void GoToPreviousUIPage(object sender, RoutedEventArgs e)
        {
            PageSwitcher.Switch(new LoginPage());
        }
        
        
    }
}
